package ru.goloshchapov.tm.exception.incorrect;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class StatusIncorrectException extends AbstractException {

    public StatusIncorrectException(@Nullable final String message) {
        super("Error! Status " + message + " is incorrect...");
    }

}

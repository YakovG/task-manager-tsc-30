package ru.goloshchapov.tm.exception.auth;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}

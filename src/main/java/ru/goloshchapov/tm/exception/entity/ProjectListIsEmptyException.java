package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class ProjectListIsEmptyException extends AbstractException {

    public ProjectListIsEmptyException() {
        super("Error! Project list is empty...");
    }

}

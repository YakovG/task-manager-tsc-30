package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class TaskNotUpdatedException extends AbstractException {

    public TaskNotUpdatedException() {
        super ("Error! Task not updated...");
    }

}

package ru.goloshchapov.tm.exception.empty;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() { super("Error! Password is empty..."); }

}

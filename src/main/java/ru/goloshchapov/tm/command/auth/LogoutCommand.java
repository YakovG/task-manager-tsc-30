package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull public static final String DESCRIPTION = "Logout from program";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }
}

package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIndexStartCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-start-by-index";

    @NotNull public static final String DESCRIPTION = "Start task by index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        @Nullable final Task task = serviceLocator.getTaskService().startOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
    }
}

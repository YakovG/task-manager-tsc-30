package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.List;

public final class UserAndProjectListCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-project-list";

    @NotNull public static final String DESCRIPTION = "Show all users with project-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USERS WITH PROJECT LIST]");
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        for (@NotNull final User user:users) {
            System.out.println(user.getLogin() + "   " + user.getRole().getDisplayName());
            serviceLocator.getProjectService().findAll(user.getId())
                    .forEach(p -> System.out.println(p.toString()));
            System.out.println("--------------------------------");
        }
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}

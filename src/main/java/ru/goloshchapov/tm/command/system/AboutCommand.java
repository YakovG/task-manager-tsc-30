package ru.goloshchapov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-a";

    @NotNull public static final String NAME = "about";

    @NotNull public static final String DESCRIPTION = "Show developer info";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}

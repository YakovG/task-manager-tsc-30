package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowArgumentsCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-arg";

    @NotNull public static final String NAME = "arguments";

    @NotNull public static final String DESCRIPTION = "Show program arguments";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final Collection<String> arguments= serviceLocator.getCommandService().getListCommandArgs();
        for (@NotNull final String arg: arguments) System.out.println(arg);
    }
}
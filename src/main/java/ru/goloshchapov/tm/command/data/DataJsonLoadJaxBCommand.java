package ru.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Domain;
import ru.goloshchapov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-json-jaxb-load";

    @NotNull public static final String DESCRIPTION = "Load data from JSON (JAXB)";

    @NotNull public static final String SYSTEM_PROPERTY_KEY = "javax.xml.bind.context.factory";

    @NotNull public static final String SYSTEM_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull public static final String UNMARSHALLER_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull public static final String UNMARSHALLER_PROPERTY_VALUE = "application/json";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        System.setProperty(SYSTEM_PROPERTY_KEY,SYSTEM_PROPERTY_VALUE);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UNMARSHALLER_PROPERTY_NAME, UNMARSHALLER_PROPERTY_VALUE);
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}

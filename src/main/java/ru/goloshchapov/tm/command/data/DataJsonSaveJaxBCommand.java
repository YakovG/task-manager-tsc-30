package ru.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Domain;
import ru.goloshchapov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-json-jaxb-save";

    @NotNull public static final String DESCRIPTION = "Save data to JSON (JAXB)";

    @NotNull public static final String SYSTEM_PROPERTY_KEY = "javax.xml.bind.context.factory";

    @NotNull public static final String SYSTEM_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull public static final String UNMARSHALLER_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull public static final String UNMARSHALLER_PROPERTY_VALUE = "application/json";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        System.setProperty(SYSTEM_PROPERTY_KEY,SYSTEM_PROPERTY_VALUE);
        @Nullable final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(UNMARSHALLER_PROPERTY_NAME, UNMARSHALLER_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}

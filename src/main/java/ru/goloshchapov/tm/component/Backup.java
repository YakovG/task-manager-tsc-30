package ru.goloshchapov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.bootstrap.Bootstrap;

public class Backup extends Thread {

    @NotNull
    private static final String COMMAND_SAVE = "backup-save";

    @NotNull
    private static final String COMMAND_LOAD = "backup-load";

    @NotNull
    private static final String COMMAND_CLEAR = "backup-clear";

    private static final int INTERVAL = 30000;

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        start();
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(COMMAND_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(COMMAND_LOAD);
    }

    @SneakyThrows
    public void delete() { bootstrap.parseCommand(COMMAND_CLEAR);}

}

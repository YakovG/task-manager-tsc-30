package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessService<M extends AbstractBusinessEntity> extends IBusinessRepository<M> {

    @Nullable
    List<M> sortedBy(String userId, String sortCheck);

    @Nullable
    M updateOneById(String userId, String modelId, String name, String description);

    @Nullable
    M updateOneByIndex(String userId, Integer index, String name, String description);

    @Nullable
    M changeOneStatusById(String userId, String id, String statusChange);

    @Nullable
    M changeOneStatusByName(String userId, String name, String statusChange);

    @Nullable
    M changeOneStatusByIndex(String userId, int index, String statusChange);

}

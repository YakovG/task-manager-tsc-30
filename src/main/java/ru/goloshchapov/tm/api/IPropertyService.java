package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull String getApplicationVersion();

}

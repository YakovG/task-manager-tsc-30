package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

public interface IUserService extends IService<User> {

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    @NotNull
    User create(String login, String password);

    @NotNull
    User create(String login, String password, String email);

    @NotNull
    User create(String login, String password, Role role);

    @NotNull
    User create(String login, String password, String email, String role);

    @NotNull
    User findUserByLogin(String login);

    @NotNull
    User findUserByEmail(String email);

    @Nullable
    User removeUser(User user);

    @NotNull
    User removeUserByLogin(String login);

    @NotNull
    User removeUserByEmail(String email);

    @NotNull
    User setPassword(String userId, String password);

    @NotNull
    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    @NotNull
    User lockUserByLogin(String login);

    @NotNull
    User unlockUserByLogin(String login);

    @NotNull
    User lockUserByEmail(String email);

    @NotNull
    User unlockUserByEmail(String email);
}
